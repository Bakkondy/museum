@extends('layouts.main')

@section('content')

    <header>
        <div class="wrapper">
            <div class="row">
                <div class="header-content">
                    <div class="logo">
                        <a href="">
                            <img src="{{asset(Voyager::image($section1->logo))}}" alt="">
                        </a>
                    </div>
                    <div class="nav-menu">
                        <ul>
                            @foreach($header as $item)
                                <li><a href="#{!! $item->href !!}">{!! $item->menu_eng !!}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="lang">
                        <ul>
                            <li>
                                <a href="{{route('index_rus')}}">Рус</a>
                            </li>
                            <li >
                                <a href="{{route('index_kaz')}}">Каз</a>
                            </li>
                            <li class="active">
                                <a href="{{route('index_eng')}}">Анг</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>


    <!-- CONTENT PART  -->
    <section class="section1" style="background: url({{asset(Voyager::image($section1->section1_bg))}});">
        <div class="wrapper">
            <div class="row">
                <div class="sec1-content">
                    <h1>{!! $section1->section1_text_eng !!}</h1>
                </div>
            </div>
        </div>
        <div class="layer"></div>
    </section>
    <section class="section2">
        <div class="wrapper">
            <div class="row">
                <div class="sec2-content">
                    <div class="sec2-1">
                        <h3>{!! $section2->title_eng !!}</h3>
                        <p>{!! $section2->description_eng !!}</p>
                    </div>
                    <div class="sec2-2">
                        <img src="{{asset(Voyager::image($section2->image))}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section3" id="gallery">
        <div class="wrapper">
            <div class="row">
                <div class="sec3-content">
                    <h2 class="heading brown">Gallery</h2>
                    <div class="tablet">
                        <ul class="tabs_menu">
                            @foreach($section3 as $item)
                                <li><span>{!! $item->tab_kaz !!}</span></li>
                            @endforeach
                        </ul>
                        <div class="tabs">
                            <div class="info active">
                                <div class="pic1 owl-carousel owl-theme">
                                    @foreach($gallery1 as $item)
                                        <div class="item" style="background-image: url({{asset(Voyager::image($item->gallery1))}});">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="info">
                                <div class="pic2 owl-carousel owl-theme">
                                    @foreach($gallery2 as $item)
                                        <div class="item" style="background-image: url({{asset(Voyager::image($item->gallery2))}});">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="info">
                                <div class="pic3 owl-carousel owl-theme">
                                    @foreach($gallery3 as $item)
                                        <div class="item" style="background-image: url({{asset(Voyager::image($item->gallery3))}});">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="layer2"></div>
    </section>
    <section class="section4" id="history">
        <div class="wrapper">
            <div class="row">
                <div class="sec4-content">
                    <h2 class="heading">History</h2>
                    <div class="history">
                        <div class="his1">
                            <span>{!! $history->title_eng !!}</span>
                            <p>{!! $history->description_eng !!}</p>
                        </div>
                        <div class="his2">
                            <img src="{{asset(Voyager::image($history->image))}}" alt="">
                            @foreach($ahmet as $item)
                                <div class="every-his">
                                    <p><span>{!! $item->title_eng !!}</span><br> {!! $item->description_eng !!}</p>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section5" id="about">
        <div class="wrapper">
            <div class="row">
                <div class="sec5-content">
                    <h2 class="heading brown">About us</h2>
                    <div class="about">
                        {{--@foreach($about as $item)--}}
                            {{--<div class="every-about">--}}
                                {{--<span>{!! $item->title_eng !!}</span>--}}
                                {{--<p>{!! $item->description_eng !!}</p>--}}
                            {{--</div>--}}
                        {{--@endforeach--}}
                            <div class="about-text">{!! $about->description_eng !!}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="layer2"></div>
    </section>
    <section class="section6">
        <div class="wrapper">
            <div class="row">
                <div class="sec6-content">
                    <h2 class="heading">Comments</h2>
                    <div class="otziv-container">
                        <div class="otziv owl-carousel owl-theme">
                            @foreach($otzyv as $item)
                                <div class="item">
                                    <div class="otz-wrap">
                                        <div class="otz1">
                                            <div class="otz-pic" style="background: url({{asset(Voyager::image($item->image))}});">

                                            </div>
                                            <div class="otz-author">
                                                <p>{!! $item->fullname_eng !!}</p>
                                            </div>
                                        </div>
                                        <div class="otz2">
                                            <div class="otz-word">
                                                {!! $item->description_eng !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section7">
        <div class="wrapper">
            <div class="row">
                <div class="sec7-content">
                    <h2 class="heading brown">Contacts</h2>
                    <div class="contacts-box">
                        <div class="cont1">
                            <div class="every-cont">
                                <img src="img/phone.png" alt="">
                                <a href="tel:{!! $contacts->phone !!}">{!! $contacts->phone !!}</a>
                            </div>
                            <div class="every-cont">
                                <img src="img/mail.png" alt="">
                                <a href="mailto:{!! $contacts->email !!}">{!! $contacts->email !!}</a>
                            </div>
                        </div>
                        <div class="cont2">
                            <iframe src="{!! $contacts->map !!}" width="100%" frameborder="1" allowfullscreen="true"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="layer2"></div>
    </section>
    <!-- CONTENT PART END  -->


    <footer>
        <div class="wrapper">
            <div class="row">
                <div class="footer">
                    <div class="foot1">
                        <p>{!! $contacts->prava_eng !!}</p>
                    </div>
                    <div class="foot2">
                        <p>{!! $contacts->year_eng !!}</p>
                    </div>
                    <div class="foot3">
                        <a href="#"><p>{!! $contacts->developer !!}</p></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

@endsection

