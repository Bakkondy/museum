<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'RusController@index')->name('index_rus');
Route::get('/kz', 'KazController@index')->name('index_kaz');
Route::get('/en', 'EngController@index')->name('index_eng');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
