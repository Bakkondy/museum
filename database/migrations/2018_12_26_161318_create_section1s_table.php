<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSection1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section1s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('logo');
            $table->string('section1_bg');
            $table->string('section1_text_rus');
            $table->string('section1_text_kaz');
            $table->string('section1_text_eng');
            $table->string('section1_text_other');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section1s');
    }
}
