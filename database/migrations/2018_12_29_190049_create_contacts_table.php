<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone');
            $table->string('email');
            $table->string('map')->nullable();
            $table->string('prava_rus');
            $table->string('prava_kaz');
            $table->string('prava_eng');
            $table->string('prava_other');
            $table->string('year_rus');
            $table->string('year_kaz');
            $table->string('year_eng');
            $table->string('year_other');
            $table->string('developer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
