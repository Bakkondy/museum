<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAhmetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ahmets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_rus');
            $table->string('title_kaz');
            $table->string('title_eng');
            $table->string('title_other');
            $table->string('description_rus');
            $table->string('description_kaz');
            $table->string('description_eng');
            $table->string('description_other');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ahmets');
    }
}
