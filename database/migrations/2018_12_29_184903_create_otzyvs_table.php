<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtzyvsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otzyvs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname_rus');
            $table->string('fullname_kaz');
            $table->string('fullname_eng');
            $table->string('fullname_other');
            $table->text('description_rus');
            $table->text('description_kaz');
            $table->text('description_eng');
            $table->text('description_other');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('otzyvs');
    }
}
