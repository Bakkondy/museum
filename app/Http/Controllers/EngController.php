<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Header;
use App\Section1;
use App\Section2;
use App\Section3;
use App\Gallery1;
use App\Gallery2;
use App\Gallery3;
use App\History;
use App\Ahmet;
use App\About;
use App\Otzyv;
use App\Contact;

class EngController extends Controller
{
    public function index()
    {
        $header = Header::all();
        $section1 = Section1::first();
        $section2 = Section2::first();
        $section3 = Section3::all();
        $gallery1 = Gallery1::all();
        $gallery2 = Gallery2::all();
        $gallery3 = Gallery3::all();
        $history = History::first();
        $ahmet = Ahmet::all();
        $about = About::first();
        $otzyv = Otzyv::all();
        $contacts = Contact::first();

        return view('museum_eng', compact(
            'header',
            'section1',
            'section2',
            'section3',
            'gallery1',
            'gallery2',
            'gallery3',
            'history',
            'ahmet',
            'about',
            'otzyv',
            'contacts'
        ));
    }
}
